import api from "./api";
import type Mac from "@/types/Machine";

const addMac = function (mac: Mac) {
  return api.post("/mac", mac);
};

const getMacs = function (): Promise<Mac[]> {
  return api.get("/mac").then((x) => x.data);
};

const getMac = function (id: number): Promise<Mac> {
  return api.get("/mac/" + id).then((x) => x.data);
};

const updateMac = function (mac: Mac, id: number) {
  return api.patch("/mac/" + id, mac);
};

const deleteMac = function (id: number) {
  return api.delete("/mac/" + id);
};

const getByComInfoId = function (id: number) {
  return api.get("/mac/cominfo/" + id).then((x) => x.data);
};

const getByIp = function (ip: string) {
  return api.get("/mac/search/" + ip).then((x) => x.data);
};

export default {
  addMac,
  getMac,
  updateMac,
  deleteMac,
  getMacs,
  getByComInfoId,
  getByIp,
};
