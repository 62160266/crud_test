import api from "./api";

function login(username: string, password: string) {
  return api.post("/auth/login", { username, password });
}

export default { login };
