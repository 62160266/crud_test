import api from "./api";
import type ComInfo from "@/types/ComInfo";

const addComInfo = function (comInfo: ComInfo) {
  return api.post("/com-info", comInfo);
};

const getComInfos = function (): Promise<ComInfo[]> {
  return api.get("/com-info").then((x) => x.data);
};

// const getComInfosByRoomId = function (id: number): Promise<ComInfo[]> {
//   return api
//     .get("http://localhost:3000/com-info/room/" + id)
//     .then((x) => x.data);
// };

const getByIp = function (ip: string): Promise<ComInfo> {
  return api.get("/com-info/search/" + ip).then((x) => x.data);
};

const getComInfosByRoomName = function (roomName: string): Promise<ComInfo[]> {
  return api.get("/com-info/room/" + roomName).then((x) => x.data);
};

const getComInfosByRoomNameAndPosition = function (
  roomName: string,
  position: number
): Promise<ComInfo[]> {
  return api
    .post("/com-info/searchPR/" + roomName, { position })
    .then((x) => x.data);
};

const getComInfo = function (id: number): Promise<ComInfo> {
  return api.get("/com-info/" + id).then((x) => x.data);
};

const updateComInfo = function (comInfo: ComInfo, id: number) {
  return api.patch("/com-info/" + id, comInfo);
};

const deleteComInfo = function (id: number) {
  return api.delete("/com-info/" + id);
};

export default {
  addComInfo,
  getComInfo,
  updateComInfo,
  deleteComInfo,
  getComInfos,
  getComInfosByRoomName,
  getComInfosByRoomNameAndPosition,
  getByIp,
};
