import api from "./api";
import type Inform from "@/types/Inform";

const addInform = function (inform: Inform) {
  return api.post("/inform", inform);
};

const getInforms = function (): Promise<Inform[]> {
  return api.get("/inform").then((x) => x.data);
};

const getInform = function (id: number): Promise<Inform> {
  return api.get("/inform/" + id).then((x) => x.data);
};

const updateInform = function (inform: Inform, id: number) {
  return api.patch("/inform/" + id, inform);
};

const deleteInform = function (id: number) {
  return api.delete("/inform/" + id);
};

const getByConfirm = function (confirm: string): Promise<Inform[]> {
  return api.get("/inform/confirm/" + confirm).then((x) => x.data);
};

const getByCF = function (confirm: string, finish: string): Promise<Inform[]> {
  return api
    .post("/inform/searchByCF/" + confirm, { finish })
    .then((x) => x.data);
};

const getByPr = function (roomName: string, position: number, finish: string) {
  return api
    .post("/inform/searchPR/" + roomName, { position, finish })
    .then((x) => x.data);
};

export default {
  addInform,
  getInforms,
  getInform,
  updateInform,
  deleteInform,
  getByConfirm,
  getByCF,
  getByPr,
};
