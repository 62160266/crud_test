import router from "@/router";
import axios from "axios";

const api = axios.create({
  baseURL: "http://playground.informatics.buu.ac.th:8999/",
  // baseURL: "http://localhost:3000/",
});

api.interceptors.request.use(
  async function (config) {
    const token = localStorage.getItem("token");
    if (token) {
      config.headers = {
        Authorization: `Bearer ${token}`,
        Accept: "application/json",
        "Content-Type": "application/json",
      };
    }

    return config;
  },
  function (error) {
    // Do something with request error
    return Promise.reject(error);
  }
);

api.interceptors.response.use(
  async function (res) {
    // await delay(1000);
    return res;
  },
  function (error) {
    // Do something with request error
    if (401 === error.response.status) {
      router.replace("/login");
    }
    return Promise.reject(error);
  }
);

export default api;
