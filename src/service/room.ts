import api from "./api";
import type Room from "@/types/Room";

const addRoom = function (room: Room) {
  return api.post("/rooms", room);
};

const getRooms = function (): Promise<Room[]> {
  return api.get("/rooms").then((x) => x.data);
};

const getRoom = function (id: number): Promise<Room> {
  return api.get("/rooms/" + id).then((x) => x.data);
};

const updateRoom = function (room: Room, id: number) {
  return api.patch("/rooms/" + id, room);
};

const deleteRoom = function (id: number) {
  return api.delete("/rooms/" + id);
};

const uploadRoomUi = function (formData: FormData) {
  return api.post("/upload", formData);
};

const getRoomUi = function (fileName: String) {
  return api.get("/upload/" + fileName).then((x) => x.data);
};

export default {
  addRoom,
  getRoom,
  updateRoom,
  deleteRoom,
  getRooms,
  uploadRoomUi,
  getRoomUi,
};
