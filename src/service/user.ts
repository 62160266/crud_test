import type User from "@/types/User";
import api from "./api";

const addUser = function (user: User) {
  return api.post("/users", user);
};

const getUser = function (): Promise<User[]> {
  return api.get("/users").then((x) => x.data);
};

const getUsers = function (id: number): Promise<User> {
  return api.get("/users/" + id).then((x) => x.data);
};

const updateUser = function (user: User, id: number) {
  return api.patch("/users/" + id, user);
};

const deleteUser = function (id: number) {
  return api.delete("/users/" + id);
};

export default { addUser, getUser, updateUser, deleteUser, getUsers };
