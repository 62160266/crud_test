import api from "./api";
import type TimeInfo from "@/types/TimeInfo";

const addTimeInfo = function (timeInfo: TimeInfo) {
  return api.post("/time-info", timeInfo);
};

const getTimeInfos = function (): Promise<TimeInfo[]> {
  return api.get("/time-info").then((x) => x.data);
};

const getByRoomNameAndPosition = function (
  roomName: string,
  position: number
): Promise<TimeInfo[]> {
  return api
    .post("/time-info/searchPR/" + roomName, { position })
    .then((x) => x.data);
};

const getTimeInfosByip = function (ip: string): Promise<TimeInfo[]> {
  return api.get("/time-info/" + ip).then((x) => x.data);
};

const getTimeInfo = function (id: number): Promise<TimeInfo> {
  return api.get("/time-info/" + id).then((x) => x.data);
};

const updateTimeInfo = function (timeInfo: TimeInfo, id: number) {
  return api.patch("/time-info/" + id, timeInfo);
};

const deleteTimeInfo = function (id: number) {
  return api.delete("/time-info/" + id);
};

export default {
  addTimeInfo,
  getTimeInfos,
  updateTimeInfo,
  deleteTimeInfo,
  getTimeInfo,
  getTimeInfosByip,
  getByRoomNameAndPosition,
};
