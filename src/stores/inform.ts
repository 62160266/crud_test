import { ref } from "vue";
import { defineStore } from "pinia";
import informService from "@/service/inform";
import comInfoService from "@/service/comInfo";
import type Inform from "@/types/Inform";
import index from "../router/index";
import api from "@/service/api";
import type ComInfo from "@/types/ComInfo";
import { comInfo } from "./cominfo";

export const inform = defineStore("inform", () => {
  const message = ref("");
  const request = false;
  const cominfos = comInfo();
  const finishs = false;
  const confirms = false;
  const confirm = "";
  const finish = "";
  const dialog = ref(false);
  const form = ref("");
  const roomName = ref("");
  const informFollow = ref<Inform>();
  const mac = ref<string>();
  const position = ref<number>();
  const allPosition: number[] = [];
  const com = ref<ComInfo>({ id: 0, ip: "", position: 0, roomName: "" });
  const getByPR = async (
    roomName: string,
    position: number,
    finish: string,
    ip: string
  ) => {
    try {
      const response = await informService.getByPr(roomName, position, finish);
      console.log(response);
      informs.value = { ...response };
      cominfos.gotoRepairHistory(ip, roomName, position);
    } catch (err) {
      console.log(err);
    }
  };
  const report = async (roomName: string) => {
    try {
      const response = await comInfoService.getComInfosByRoomName(roomName);
      for (let i = 0; i < response.length; i++) {
        allPosition.push(response[i].position!);
      }
    } catch (error) {
      console.log(error);
    }
  };
  const getLocal = async () => {
    await api.get("/realIp").then(async (x) => {
      if (x.data) {
        com.value = { ...x.data };
      } else {
        com.value.id = 1;
        com.value.ip = "192.168.1.40";
        com.value.position = 1;
        com.value.roomName = "3C01";
        com.value.mac = "8C8CAA4D6653";
        position.value = com.value!.position;
        mac.value = com.value!.mac;
        console.log(com.value);
        report(com.value!.roomName!);
      }
    });
  };
  const getInform = async (id: number) => {
    try {
      const response = await informService.getInform(id);
      informFollow.value = { ...response };
    } catch (error) {
      console.log(error);
    }
  };
  const gotoInform = async () => {
    dialog.value = true;
    form.value = "";
  };
  const addForm = ref<Inform>({
    form: "",
    confirm: "",
    finish: "",
    position: undefined,
    roomName: "",
  });
  const updateForm = ref<Inform>({
    form: "",
    confirm: "",
    finish: "",
    position: undefined,
    roomName: "",
  });
  const informs = ref<Inform[]>();
  const inform = ref<Inform>();
  const addInform = async (roomName: string) => {
    let minute: string;
    let second: string;
    if (new Date().getMinutes().toString().length === 1) {
      minute = "0" + new Date().getMinutes().toString();
    }
    if (new Date().getSeconds().toString().length === 1) {
      second = "0" + new Date().getSeconds().toString();
    }
    if (new Date().getMinutes().toString().length === 2) {
      minute = new Date().getMinutes().toString();
    }
    if (new Date().getSeconds().toString().length === 2) {
      second = new Date().getSeconds().toString();
    }
    addForm.value.form = form.value;
    addForm.value.confirm = "false";
    addForm.value.finish = "false";
    addForm.value.position = position.value;
    addForm.value.mac = mac.value;
    addForm.value.roomName = roomName;
    addForm.value.newTime =
      new Date().getDate().toString() +
      "/" +
      (new Date().getMonth() + 1).toString() +
      "/" +
      new Date().getFullYear().toString() +
      " " +
      new Date().getHours().toString() +
      ":" +
      minute! +
      ":" +
      second!;

    addForm.value.message = "";
    try {
      const response = await comInfoService.getComInfosByRoomNameAndPosition(
        roomName,
        position.value!
      );

      console.log(response);
      if (response.length !== 0) {
        await informService.addInform(addForm.value!);
        const response = await informService.getInforms();
        alert(JSON.stringify("ทำการแจ้งเรื่องสำเร็จ"));

        index.push("/follow/" + response[response.length - 1].id);
      } else {
        alert(JSON.stringify("ไม่มีห้องนี้ หรือไม่มีตำแหน่งในห้องนี้"));
        return;
      }
    } catch (error) {
      console.log(error);
    }
    dialog.value = false;
  };
  const getById = async (id: number) => {
    try {
      const response = await informService.getInform(id);
      // console.log(response);
      inform.value = { ...response };
    } catch (error) {
      console.log(error);
    }
  };
  const getByConfirm = async (confirm: string) => {
    try {
      const response = await informService.getByConfirm(confirm);
      // console.log(response);
      informs.value = { ...response };
    } catch (error) {
      console.log(error);
    }
  };
  const comment = async (id: number) => {
    dialog.value = true;
    getById(id);
  };
  const updateConfirm = async (id: number, status: string) => {
    try {
      const response = await informService.getInform(id);
      updateForm.value = { ...response };
      updateForm.value.confirm = status;
      if (status === "true") {
        let minute: string;
        let second: string;
        if (new Date().getMinutes().toString().length === 1) {
          minute = "0" + new Date().getMinutes().toString();
        }
        if (new Date().getSeconds().toString().length === 1) {
          second = "0" + new Date().getSeconds().toString();
        }
        if (new Date().getMinutes().toString().length === 2) {
          minute = new Date().getMinutes().toString();
        }
        if (new Date().getSeconds().toString().length === 2) {
          second = new Date().getSeconds().toString();
        }
        updateForm.value.finishTime =
          new Date().getDate().toString() +
          "/" +
          (new Date().getMonth() + 1).toString() +
          "/" +
          new Date().getFullYear().toString() +
          " " +
          new Date().getHours().toString() +
          ":" +
          minute! +
          ":" +
          second!;
        alert(JSON.stringify("เสร็จสิ้นแล้ว"));
      } else {
        updateForm.value.finishTime = "ยกเลิกแล้ว";
        alert(JSON.stringify("ยกเลิกแล้ว"));
      }
      await informService.updateInform(updateForm.value, id);
      getByConfirm("false");
    } catch (error) {
      console.log(error);
    }
  };

  const getByCF = async (confirm: string, finish: string) => {
    try {
      console.log(confirm + " " + finish);
      const response = await informService.getByCF(confirm, finish);
      console.log(response);
      informs.value = { ...response };
    } catch (error) {
      console.log(error);
    }
  };

  const finishConfirm = async (id: number, status: string) => {
    try {
      const response = await informService.getInform(id);
      updateForm.value = { ...response };
      updateForm.value.finish = status;

      if (status === "true") {
        let minute: string;
        let second: string;
        if (new Date().getMinutes().toString().length === 1) {
          minute = "0" + new Date().getMinutes().toString();
        }
        if (new Date().getSeconds().toString().length === 1) {
          second = "0" + new Date().getSeconds().toString();
        }
        if (new Date().getMinutes().toString().length === 2) {
          minute = new Date().getMinutes().toString();
        }
        if (new Date().getSeconds().toString().length === 2) {
          second = new Date().getSeconds().toString();
        }
        updateForm.value.finishTime =
          new Date().getDate().toString() +
          "/" +
          (new Date().getMonth() + 1).toString() +
          "/" +
          new Date().getFullYear().toString() +
          " " +
          new Date().getHours().toString() +
          ":" +
          minute! +
          ":" +
          second!;
        alert(JSON.stringify("เสร็จสิ้นแล้ว"));
      } else {
        updateForm.value.finishTime = "ยกเลิกแล้ว";
        alert(JSON.stringify("ยกเลิกแล้ว"));
      }
      await informService.updateInform(updateForm.value, id);
      getByCF("true", "false");
    } catch (error) {
      console.log(error);
    }
  };
  const updateComment = async () => {
    try {
      if (inform.value) {
        if (!inform.value.message) {
          inform.value.message = message.value!;
        } else {
          inform.value.message += ", " + message.value!;
        }
        await informService.updateInform(inform.value!, inform.value?.id!);
      }
      alert(JSON.stringify("เพิ่มความคิดเห็นแล้ว"));
    } catch (error) {
      console.log(error);
    }
    dialog.value = false;
    message.value = "";
  };
  return {
    dialog,
    form,
    roomName,
    position,
    gotoInform,
    addInform,
    informs,
    getByConfirm,
    updateConfirm,
    finishConfirm,
    getByCF,
    allPosition,
    informFollow,
    getInform,
    report,
    finish,
    confirm,
    getLocal,
    com,
    request,
    confirms,
    comment,
    finishs,
    inform,
    getById,
    updateComment,
    message,
    updateForm,
    getByPR,
  };
});
