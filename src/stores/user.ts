import { ref } from "vue";
import { defineStore } from "pinia";
import index from "../router/index";
import userService from "../service/user";
import type User from "@/types/User";
// import { useAppStore } from "./app";

export const auther = defineStore("user", () => {
  // const app = useAppStore();
  // const isLogin = ref(false);
  const changePass = ref(false);
  const dialog = ref(false);
  const drawer = ref(false);
  const selectedItem = ref(1);
  const confirmPass = ref("");
  const items = ref([
    { text: "My Files", icon: "mdi-folder" },
    { text: "Shared with me", icon: "mdi-account-multiple" },
    { text: "Starred", icon: "mdi-star" },
    { text: "Recent", icon: "mdi-history" },
    { text: "Offline", icon: "mdi-check-circle" },
    { text: "Uploads", icon: "mdi-upload" },
    { text: "Backups", icon: "mdi-cloud-upload" },
  ]);

  const goBack = () => {
    index.push("/");
  };

  const gotoChangePass = () => {
    changePass.value = true;
    dialog.value = true;
    changePassUser.value.password = "";
    confirmPass.value = "";
  };

  const appTitle = ref("Awesome App");
  const sidebar = ref(false);
  const menuItems = ref([
    { title: "หน้าหลัก", path: "/", icon: "mdi-home-circle" },
    { title: "ข้อมูลห้อง", path: "/room", icon: "mdi-database" },
    { title: "เรื่องที่แจ้งเข้ามา", path: "/request", icon: "mdi-bug" },
    // { title: "รับเรื่องแล้ว", path: "/finish", icon: "mdi-file-chart" },
    // { title: "Logout", path: "/login", icon: "exit" },
  ]);
  const users = ref({
    username: "",
    password: "",
  });
  const changePassUser = ref<User>({
    id: 0,
    name: "",
    surname: "",
    email: "",
    username: "",
    password: "",
  });
  const allUser = ref<User[]>();
  const user = ref<User>();
  const idValue = ref<number>(0);

  const addUser = async () => {
    // app.load();
    if (users.value.username === "" || users.value.password === "") {
      alert(JSON.stringify("please input user and password"));
    } else {
      try {
        await userService.addUser(users.value);
        // app.unload();
      } catch (error) {
        console.log(error);
        // app.unload();
      }
      index.push("/");
    }
    getUser();
  };

  const gotoAdd = (id: number) => {
    idValue.value = id;
    if (id !== 0) {
      getUser();
    } else {
      users.value.username = "";
      users.value.password = "";
    }
    index.push("/form");
  };

  const gotoRoomUi = () => {
    index.push("/room");
  };

  const submit = () => {
    if (idValue.value === 0) {
      addUser();
    } else {
      updateUser();
    }
  };

  // const setText = () => {
  //   users.value.username = update.value.username;
  //   users.value.password = update.value.password;
  // };

  const getUser = async () => {
    try {
      const id = localStorage.getItem("id");
      if (id !== null) {
        const response = await userService.getUsers(+id);
        user.value = response;
        // setText();
        console.log(user.value);
      }
    } catch (error) {
      console.log(error);
    }
  };

  const deleteUsers = async (id: number) => {
    try {
      await userService.deleteUser(id);
      getUser();
    } catch (error) {
      console.log(error);
    }
  };

  const getUsers = async () => {
    try {
      const response = await userService.getUser();
      allUser.value = response;
      console.log(response);
    } catch (error) {
      console.log(error);
    }
  };

  const updateUser = async () => {
    if (changePassUser.value!.password! === confirmPass.value!) {
      changePassUser.value.email = user.value?.email;
      changePassUser.value.id = user.value?.id;
      changePassUser.value.name = user.value?.name;
      changePassUser.value.surname = user.value?.surname;
      changePassUser.value.username = user.value?.username;
      try {
        await userService.updateUser(
          changePassUser.value,
          changePassUser.value!.id!
        );
        changePass.value = false;
        dialog.value = false;
        alert("รหัสผ่านถูกเปลี่ยน");
      } catch (error) {
        console.log(error);
      }
    } else {
      alert("รหัสผ่านไม่เหมือนกัน");
    }
  };

  return {
    users,
    submit,
    idValue,
    gotoAdd,
    getUser,
    allUser,
    addUser,
    getUsers,
    deleteUsers,
    updateUser,
    user,
    // setText,
    gotoRoomUi,
    selectedItem,
    items,
    appTitle,
    sidebar,
    menuItems,
    drawer,
    dialog,
    changePass,
    confirmPass,
    changePassUser,
    gotoChangePass,
    goBack,
  };
});
