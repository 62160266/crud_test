import { rooms } from "./room";
import { defineStore } from "pinia";
import { ref } from "vue";
import cominfoService from "../service/comInfo";
import roomService from "../service/room";
import timeInfoService from "../service/timeInfo";
import index from "../router/index";
import type ComInfo from "@/types/ComInfo";
import type Room from "@/types/Room";
import type TimeInfo from "@/types/TimeInfo";

export const comInfo = defineStore("cominfo", () => {
  const room = rooms();
  const time = ref<TimeInfo[]>();
  const ip = ref("");
  const setIp = ref<ComInfo>({ id: 0, position: 0, ip: "", roomName: "" });
  const addUpdate = ref(false);
  const dialog = ref(false);
  const comInfos = ref<ComInfo[] | null>();
  const allCom = ref(0);
  const files = ref<File>();
  const addCom = ref<ComInfo | null>({ id: 0, ip: "", position: 0 });
  const checkCom = ref<ComInfo>();
  const size = ref(0);
  // const checkHour = ref<Boolean>();
  const dayInMonth = ref<number>();
  const timeCheck: string[] = [];

  const allTimeCheck: string[][] = [];

  const gotoAdd = () => {
    addUpdate.value = false;
    // roomId.value = 0;
    addCom.value!.ip = "";
    addCom.value!.id = 0;
    addCom.value!.position = 0;
    dialog.value = true;
  };

  const roomCheck = ref<Room | null>();
  const getRooms = async () => {
    try {
      const response = await roomService.getRoom(room.roomId);
      roomCheck.value = response;
      console.log(roomCheck.value.roomName);
    } catch (error) {
      console.log(error);
    }
  };

  // function checkRoom(roomname: string) {}

  function gotoMachine(ip: string, roomName: string) {
    setIp.value.ip = ip;
    index.push("/mac/" + roomName + "/?ip=" + ip);
  }

  function gotoRepairHistory(ip: string, roomName: string, position: number) {
    setIp.value.ip = ip;
    index.push("/history/" + roomName + "/" + position);
  }

  const getComInfos = async (roomName: string) => {
    try {
      const response = await cominfoService.getComInfosByRoomName(roomName);

      allCom.value = response.length;
      comInfos.value = { ...response };
    } catch (error) {
      console.log(error);
    }
  };

  const allCheck = async (roomName: string, allCom: number) => {
    try {
      allTimeCheck.splice(0, allTimeCheck.length);
      for (let p = 1; p <= allCom; p++) {
        const response = await timeInfoService.getByRoomNameAndPosition(
          roomName,
          p
        );

        time.value = { ...response };
        size.value = response.length - 1;
        if (room.selectOption) {
          if (
            room.month.month === 0 ||
            room.month.month === 2 ||
            room.month.month === 4 ||
            room.month.month === 6 ||
            room.month.month === 7 ||
            room.month.month === 9 ||
            room.month.month === 11
          ) {
            dayInMonth.value = 31;
          }
          if (
            room.month.month === 3 ||
            room.month.month === 5 ||
            room.month.month === 8 ||
            room.month.month === 10
          ) {
            dayInMonth.value = 30;
          }
          if (room.month.month === 1) {
            dayInMonth.value = 29;
          }
          console.log(dayInMonth.value);
          for (let l = 1; l <= dayInMonth.value!; l++) {
            if (response.length === 0) {
              timeCheck.splice(l - 1, 1, "X");
            } else {
              for (let i = 0; i < response.length; i++) {
                if (
                  +response[i].month! === room.month.month + 1 &&
                  +response[i].year! === room.month.year
                ) {
                  if (response[i].day === l) {
                    if (
                      response[i].hour! === response[i].shutdownHour! &&
                      response[i].shutdownMinute! - response[i].minute! >= 10
                    ) {
                      timeCheck.splice(l - 1, 1, "O");
                      break;
                    }
                    if (
                      response[i].shutdownHour! - response[i].hour! === 1 &&
                      60 - response[i].shutdownMinute! + response[i].minute! >=
                        10
                    ) {
                      timeCheck.splice(l - 1, 1, "O");
                      break;
                    }
                    if (response[i].shutdownHour! - response[i].hour! >= 2) {
                      timeCheck.splice(l - 1, 1, "O");
                      break;
                    } else {
                      timeCheck.splice(l - 1, 1, "X");
                      break;
                    }
                  } else {
                    timeCheck.splice(l - 1, 1, "X");
                  }
                } else {
                  timeCheck.splice(l - 1, 1, "X");
                }
              }
              console.log(timeCheck);
            }
          }
        } else {
          for (let l = 1; l <= 24; l++) {
            if (response.length === 0) {
              timeCheck.splice(l - 1, 1, "X");
            } else {
              for (let i = 0; i < response.length; i++) {
                if (
                  +response[i].month! === room.date.getMonth() + 1 &&
                  +response[i].day! === +room.date.getDate() &&
                  +response[i].year! === room.date.getFullYear()
                ) {
                  if (response[i].hour === l) {
                    if (
                      response[i].hour! === response[i].shutdownHour! &&
                      response[i].shutdownMinute! - response[i].minute! >= 10
                    ) {
                      for (
                        let f = response[i].hour!;
                        f <= response[i].shutdownHour!;
                        f++
                      ) {
                        timeCheck.splice(l - 1, 1, "O");
                        l += 1;
                      }
                      timeCheck.splice(l, 1, "X");
                    }
                    if (
                      response[i].shutdownHour! - response[i].hour! === 1 &&
                      60 - response[i].shutdownMinute! + response[i].minute! >=
                        10
                    ) {
                      for (
                        let f = response[i].hour!;
                        f <= response[i].shutdownHour!;
                        f++
                      ) {
                        timeCheck.splice(l - 1, 1, "O");
                        l += 1;
                      }
                      timeCheck.splice(l, 1, "X");
                    }
                    if (response[i].shutdownHour! - response[i].hour! >= 2) {
                      for (
                        let f = response[i].hour!;
                        f <= response[i].shutdownHour!;
                        f++
                      ) {
                        timeCheck.splice(l - 1, 1, "O");
                        l += 1;
                      }
                      timeCheck.splice(l, 1, "X");
                    } else {
                      for (
                        let f = response[i].hour!;
                        f <= response[i].shutdownHour!;
                        f++
                      ) {
                        timeCheck.splice(l - 1, 1, "X");
                        l += 1;
                      }
                      timeCheck.splice(l, 1, "X");
                    }
                  } else {
                    timeCheck.splice(l - 1, 1, "X");
                  }
                } else {
                  timeCheck.splice(l - 1, 1, "X");
                }
              }
            }
          }
        }
        allTimeCheck.push(timeCheck.splice(0, timeCheck.length));
      }
      // console.log(allTimeCheck);
    } catch (err) {
      console.log(err);
    }
  };

  const getComInfo = async (id: number) => {
    try {
      const response = await cominfoService.getComInfo(id);
      addCom.value = response;
      console.log(response);
    } catch (error) {
      console.log(error);
    }
  };

  const getComInfoByIp = async (ip: string) => {
    try {
      const response = await cominfoService.getByIp(ip);
      addCom.value = response;
    } catch (error) {
      console.log(error);
    }
  };

  const deleteCom = async (id: number) => {
    try {
      await cominfoService.deleteComInfo(id);
      alert(JSON.stringify("computer id " + id + " has been delete"));
      getComInfos(room.roomName.roomname);
    } catch (error) {
      console.log(error);
    }
  };

  const addComInfo = async () => {
    if (dialog.value === false) {
      addCom.value!.ip = "";
      addCom.value!.position = 0;
    }
    if (addCom.value!.ip === "" && addCom.value!.position === 0) {
      alert(JSON.stringify("please input ip and computer position"));
    } else {
      try {
        await room.getRoom();
        // room.getRoom(room.roomId);
        //รอถามอาจารย์
        if (addCom.value) {
          addCom.value!.roomName = room.setRoom.roomName;
        }

        await cominfoService.addComInfo(addCom.value!);
        console.log(addCom.value);
      } catch (error) {
        console.log(error);
        // app.unload();
      }
      addCom.value!.ip = "";
      addCom.value!.position = 0;
      addCom.value!.roomName = "";
      room.setRoom!.id = 0;
      room.setRoom!.roomName = "";
      getComInfos(room.roomName.roomname);
      dialog.value = false;
    }
  };

  const submitFile = async () => {
    console.log(files.value);
    if (files.value === undefined) return;
    const formData = new FormData();
    formData.append("file", files.value, files.value.name);
    try {
      // const response = await roomService.uploadRoomUi(formData);
      const response = await fetch(
        "http://playground.informatics.buu.ac.th:8999/uploadTable",
        {
          method: "POST",
          body: formData,
        }
      );
      if (!response.ok) {
        throw new Error(response.statusText);
      }
      console.log(response);
    } catch (err) {
      console.log(err);
    }
    getComInfos(room.roomName.roomname);
    dialog.value = false;
  };

  const updateComInfo = async () => {
    try {
      await cominfoService.updateComInfo(addCom.value!, addCom.value!.id!);
    } catch (error) {
      console.log(error);
    }
    // room.value.roomname = "";
    alert(
      JSON.stringify("computer id " + addCom.value!.id + " has been update")
    );
    getComInfos(room.roomName.roomname);
    dialog.value = false;
  };

  const gotoUpdate = (id: number) => {
    addUpdate.value = true;
    if (id !== 0) {
      getComInfo(id);
    }
    dialog.value = true;
  };

  return {
    comInfos,
    getComInfos,
    timeCheck,
    getRooms,
    roomCheck,
    gotoMachine,
    setIp,
    dialog,
    addCom,
    gotoAdd,
    deleteCom,
    addComInfo,
    updateComInfo,
    gotoUpdate,
    addUpdate,
    submitFile,
    files,
    allCom,
    checkCom,
    allCheck,
    ip,
    time,
    size,
    allTimeCheck,
    dayInMonth,
    getComInfoByIp,
    gotoRepairHistory,
  };
});
