import { ref } from "vue";
import { defineStore } from "pinia";

export const useAppStore = defineStore("application", () => {
  const loading = ref(false);
  const load = function () {
    loading.value = true;
  };
  const unload = function () {
    loading.value = false;
  };
  return {
    loading,
    load,
    unload,
  };
});
