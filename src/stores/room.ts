import { defineStore } from "pinia";
import index from "../router/index";
import roomService from "../service/room";
import { ref } from "vue";
import type Room from "@/types/Room";
import { comInfo } from "../stores/cominfo";
import { userAuth } from "../stores/login";

// import ComInfo from "@/types/ComInfo";

export const rooms = defineStore("room", () => {
  const date = ref(new Date());
  const month = ref({ month: 0, year: 0 });
  const selectOption = ref(false);
  const cominfo = comInfo();
  const loading = userAuth();
  const daySelect = ref<string>();
  const monthSelect = ref<string>();
  const yearSelect = ref("");
  const roomId = ref(0);
  const setRoom = ref<Room>({
    id: 0,
    roomName: "",
  });
  const roomName = ref({ roomname: "" });
  // const idValue = ref(0);
  const selectViewDialog = ref(false);
  const dialog = ref(false);
  const addUpdate = ref(false);
  const files = ref<File>();
  const UI = ref<String[][] | null>();
  const room = ref<Room[] | null>();
  const submitFile = async () => {
    if (files.value === undefined) return;
    const formData = new FormData();
    formData.append("file", files.value, files.value.name);
    try {
      // const response = await roomService.uploadRoomUi(formData);
      const response = await fetch(
        "http://playground.informatics.buu.ac.th:8999/upload",
        {
          method: "POST",
          body: formData,
        }
      );
      if (!response.ok) {
        throw new Error(response.statusText);
      }
      console.log(response);
    } catch (err) {
      console.log(err);
    }
  };

  const getRoomUi = async () => {
    try {
      const response = await roomService.getRoomUi(
        roomName.value.roomname + ".xlsx"
      );
      UI.value = response;
    } catch (error) {
      alert(JSON.stringify("ห้องนี้ไม่มีไฟล์ UI"));
      console.log(error);
    }
  };

  const gotoUpdate = (id: number) => {
    addUpdate.value = true;
    roomId.value = id;
    if (id !== 0) {
      getRoom();
    }
    dialog.value = true;
  };

  const gotoAdd = () => {
    addUpdate.value = false;
    roomId.value = 0;
    setRoom.value = { id: 0, roomName: "" };
    dialog.value = true;
  };

  const getRoom = async () => {
    try {
      const response = await roomService.getRoom(roomId.value);
      setRoom.value = { id: response.id, roomName: response.roomName };
    } catch (error) {
      console.log(error);
    }
  };

  const getRooms = async () => {
    try {
      const response = await roomService.getRooms();
      room.value = { ...response };
      console.log(response);
    } catch (error) {
      console.log(error);
    }
  };

  const addRoom = async () => {
    if (dialog.value === false) {
      setRoom.value!.roomName = "";
    }
    if (setRoom.value!.roomName === "") {
      alert(JSON.stringify("please input room name"));
    } else {
      try {
        await roomService.addRoom(setRoom.value!);
        console.log(room.value);
      } catch (error) {
        console.log(error);
        // app.unload();
      }
    }
    setRoom.value = { id: 0, roomName: "" };
    getRooms();
    dialog.value = false;
  };

  const updateRoom = async () => {
    try {
      await roomService.updateRoom(setRoom.value!, setRoom.value!.id!);

      alert(
        JSON.stringify("room id " + setRoom.value!.id + " has been update")
      );
      getRooms();
      dialog.value = false;
    } catch (error) {
      console.log(error);
    }
    // room.value.roomname = "";
  };

  const gotoComInfo = (id: number, roomname: string) => {
    roomId.value = id;
    roomName.value.roomname = roomname;
    index.push("/cominfo/" + roomname);
  };

  const gotoSelectView = (select: number) => {
    selectViewDialog.value = true;
    date.value = new Date();
    month.value.month = date.value.getMonth();
    month.value.year = date.value.getFullYear();
    console.log(select);
    if (select === 1) {
      selectOption.value = false;
    }
    if (select === 2) {
      selectOption.value = true;
    }
  };

  const gotoCheckCom = (roomname: string) => {
    roomName.value.roomname = roomname;
    if (selectOption.value) {
      if (date.value === null) {
        alert(JSON.stringify("กรุณาเลือกเดือนก่อน"));
      } else {
        cominfo.allCheck(roomname, cominfo.allCom);
        loading.dialog = true;
        setTimeout(() => {
          loading.dialog = false;
          index.push("/checkCom/" + roomname);
        }, 2000);
      }
    } else {
      if (date.value === null) {
        alert(JSON.stringify("กรุณาเลือกวันก่อน"));
      } else {
        cominfo.allCheck(roomname, cominfo.allCom);
        loading.dialog = true;
        setTimeout(() => {
          loading.dialog = false;
          index.push("/checkCom/" + roomname);
        }, 2000);
      }
    }
  };

  const deleteRoom = async (id: number) => {
    try {
      await roomService.deleteRoom(id);
      alert(JSON.stringify("room id " + id + " has been delete"));
      getRooms();
    } catch (error) {
      console.log(error);
    }
  };

  const roomUi = true;
  const table = false;

  function back() {
    index.push("/room");
  }

  function backToMain() {
    index.push("/");
  }

  return {
    back,
    room,
    roomUi,
    table,
    getRooms,
    gotoComInfo,
    roomId,
    backToMain,
    setRoom,
    dialog,
    addRoom,
    deleteRoom,
    getRoom,
    selectOption,
    addUpdate,
    gotoUpdate,
    gotoAdd,
    updateRoom,
    roomName,
    files,
    submitFile,
    getRoomUi,
    UI,
    gotoCheckCom,
    selectViewDialog,
    daySelect,
    monthSelect,
    yearSelect,
    gotoSelectView,
    date,
    month,
  };
});
