import { defineStore } from "pinia";
import macService from "../service/machine";
import { ref } from "vue";
import { comInfo } from "../stores/cominfo";
import type Machine from "@/types/Machine";
import index from "../router/index";
import { userAuth } from "./login";

export const machine = defineStore("mac", () => {
  const loading = userAuth();
  const comInfos = comInfo();
  const allMachine = ref<Machine[]>();
  const getMacsByIp = async (ip: string) => {
    try {
      const response = await macService.getByIp(ip);
      allMachine.value = response;
      comInfos.setIp.ip = ip;
    } catch (error) {
      console.log(error);
    }
  };

  const getMacForCheck = async () => {
    try {
      console.log(comInfos.ip!);
      const response = await macService.getByIp(comInfos.ip!);
      allMachine.value = { ...response };
      console.log(response);
    } catch (error) {
      console.log(error);
    }
  };

  function back(roomName: string) {
    comInfos.timeCheck.splice(0, 31);
    loading.dialog = true;
    index.push("/cominfo/" + roomName);
    setTimeout(() => {
      loading.dialog = false;

      window.location.reload();
    }, 2000);
  }

  return { getMacsByIp, allMachine, back, getMacForCheck };
});
