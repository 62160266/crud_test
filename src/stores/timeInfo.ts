import type TimeInfo from "@/types/TimeInfo";
import timeInfoService from "../service/timeInfo";
import { defineStore } from "pinia";
import { ref } from "vue";

export const timeInfo = defineStore("timeinfo", () => {
  const timeinfos = ref<TimeInfo[] | null>();
  const timeinfo = ref<TimeInfo | null>();

  const getTimeInfos = async (ip: string) => {
    try {
      const response = await timeInfoService.getTimeInfosByip(ip);
      console.log(response);
    } catch (error) {
      console.log(error);
    }
  };
  return { timeinfos, timeinfo, getTimeInfos };
});
