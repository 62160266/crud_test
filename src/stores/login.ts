import { defineStore } from "pinia";
import { ref } from "vue";
import auth from "../service/auth";
import router from "../router/index";

export const userAuth = defineStore("auth", () => {
  const isLogin = ref(false);
  const authName = ref("");
  const dialog = ref(false);
  const gotoUser = () => {
    router.push("/user");
  };

  const login = async (userName: string, password: string): Promise<void> => {
    try {
      const res = await auth.login(userName, password);
      console.log(res);
      if (res.status === 201) {
        dialog.value = true;
        setTimeout(() => {
          localStorage.setItem("user", JSON.stringify(res.data.user));
          localStorage.setItem("id", JSON.stringify(res.data.user.id));
          localStorage.setItem("token", res.data.access_token);
          router.push("/");
          isLogin.value = true;
          dialog.value = false;
        }, 2000);
      }
    } catch (err) {
      alert("Username หรือ Password ไม่ถูกต้อง");
      isLogin.value = false;
    }
    // authName.value = userName;
    // localStorage.setItem("authName", userName);
  };
  const logout = () => {
    // authName.value = "";
    dialog.value = true;
    setTimeout(() => {
      isLogin.value = false;
      localStorage.removeItem("id");
      localStorage.removeItem("user");
      localStorage.removeItem("token");
      router.push("/");
      dialog.value = false;
    }, 2000);
  };

  return { authName, login, logout, isLogin, gotoUser, dialog };
});
