import AllRoomVue from "@/views/AllRoom.vue";
import { createRouter, createWebHistory } from "vue-router";
import HomeView from "../views/HomeView.vue";
import RoomTAndU from "../views/RoomTAndU.vue";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/",
      name: "home",
      components: {
        default: HomeView,
        menu: () => import("@/components/MenuView.vue"),
        header: () => import("@/components/MainHead.vue"),
      },
      meta: {
        layout: "MainLayout",
      },
    },
    {
      path: "/room",
      name: "room",
      components: {
        default: AllRoomVue,
        menu: () => import("@/components/MenuView.vue"),
        header: () => import("@/components/MainHead.vue"),
      },
      meta: {
        layout: "MainLayout",
        requiresAuth: true,
      },
    },
    {
      path: "/cominfo/:roomName",
      name: "cominfo",
      components: {
        default: RoomTAndU,
        menu: () => import("@/components/MenuView.vue"),
        header: () => import("@/components/MainHead.vue"),
      },
      meta: {
        layout: "MainLayout",
        requiresAuth: true,
      },
    },
    {
      path: "/mac/:roomName",
      name: "machine",
      components: {
        default: () => import("../views/MacTable.vue"),
        menu: () => import("@/components/MenuView.vue"),
        header: () => import("@/components/MainHead.vue"),
      },
      meta: {
        layout: "MainLayout",
        requiresAuth: true,
      },
    },
    {
      path: "/user",
      name: "user",
      components: {
        default: () => import("../views/UserInfoVue.vue"),
        menu: () => import("@/components/MenuView.vue"),
        header: () => import("@/components/MainHead.vue"),
      },
      meta: {
        layout: "MainLayout",
        requiresAuth: true,
      },
    },
    {
      path: "/login",
      name: "login",
      components: {
        default: () => import("../views/LoginView.vue"),
      },
      meta: { layout: "FullLayout" },
    },
    {
      path: "/checkCom/:roomName",
      name: "checkCom",
      components: {
        default: () => import("../views/ComList.vue"),
        menu: () => import("@/components/MenuView.vue"),
        header: () => import("@/components/MainHead.vue"),
      },
      meta: {
        layout: "MainLayout",
        requiresAuth: true,
      },
    },
    {
      path: "/request",
      name: "request",
      components: {
        default: () => import("../views/RequestView.vue"),
        menu: () => import("@/components/MenuView.vue"),
        header: () => import("@/components/MainHead.vue"),
      },
      meta: {
        layout: "MainLayout",
        requiresAuth: true,
      },
    },
    {
      path: "/history/:roomName/:position",
      name: "history",
      components: {
        default: () => import("../views/HistoryView.vue"),
        menu: () => import("@/components/MenuView.vue"),
        header: () => import("@/components/MainHead.vue"),
      },
      meta: {
        layout: "MainLayout",
        requiresAuth: true,
      },
    },
    {
      path: "/follow/:id",
      name: "follow",
      components: {
        default: () => import("../views/FollowView.vue"),
        menu: () => import("@/components/MenuView.vue"),
        header: () => import("@/components/MainHead.vue"),
      },
      meta: {
        layout: "MainLayout",
      },
    },
  ],
});
const isLogin = () => {
  const user = localStorage.getItem("user");
  if (user) {
    return true;
  }
  return false;
};
router.beforeEach((to) => {
  // instead of having to check every route record with
  // to.matched.some(record => record.meta.requiresAuth)
  if (to.meta.requiresAuth && !isLogin()) {
    // this route requires auth, check if logged in
    // if not, redirect to login page.
    return {
      path: "/login",
      // save the location we were at to come back later
      query: { redirect: to.fullPath },
    };
  }
});
export default router;
