export default interface Inform {
  id?: number;
  roomName?: string;
  position?: number;
  form?: string;
  confirm?: string;
  finish?: string;
  newTime?: string;
  finishTime?: string;
  confirmTime?: string;
  message?: string;
  mac?: string;
}
