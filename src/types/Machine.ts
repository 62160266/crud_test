export default interface Machine {
  id?: number;
  ip?: string;
  mac?: string;
  timeOpen?: string;
  lastTimeShutdown?: string;
  cpu?: string;
  mainboard?: string;
  ram?: string;
}
