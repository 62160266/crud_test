export default interface ComInfo {
  id?: number;
  ip?: string;
  position?: number;
  roomName?: string;
  mac?: string;
}
