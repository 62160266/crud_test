export default interface TimeInfo {
  id?: number;
  ip?: string;
  year?: string;
  month?: string;
  day?: number;
  hour?: number;
  minute?: number;
  shutdownHour?: number;
  shutdownMinute?: number;
  second?: number;
}
